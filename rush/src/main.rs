#![allow(unused_imports)]

use libc;
use rush::{string_to_cstr, cstr_to_string};
use std::ptr::{null, null_mut};
use std::io;

fn main() {
    loop {
        let mut cmd = String::new();
        eprint!("rush> ");
        io::stdin().read_line(&mut cmd).expect("unexpected reading failure!");
        eprintln!(" -- your command was: {}", cmd);
        let cmd: Vec<&str> = cmd.trim().split_whitespace().collect();

        if cmd.len() < 1 {
            eprintln!("expected command!");
            continue;
        }
        let executable = cmd[0];
        let args = &cmd[..];
        if executable == "exit" {
            return;
        }

        unsafe {
            let executable = string_to_cstr(executable);
            let args: Vec<Vec<i8>> = args.iter().map(|&x| string_to_cstr(x)).collect();
            let args: Vec<*const i8> = args.iter().map(|x| x.as_ptr()).chain(vec![null()]).collect();
            let pid = libc::fork();
            if pid < 0 {
                eprintln!("failed forking process: {}",
                          cstr_to_string(libc::strerror(*libc::__errno_location())).unwrap());
            } else if pid == 0 {
                if libc::execvp(executable.as_ptr(), args.as_ptr()) < 0 {
                    eprintln!("failed executing command: {}",
                              cstr_to_string(libc::strerror(*libc::__errno_location())).unwrap());
                }
            } else {
                libc::waitpid(pid, null_mut(), 0);
            }
        }
    }
}
