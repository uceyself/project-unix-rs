use std::string::FromUtf8Error;
use std::ptr::null;

pub fn string_to_cstr(s: &str) -> Vec<i8> {
    s.as_bytes().iter().map(|&x| x as i8).chain(vec![0]).collect()
}

pub unsafe fn cstr_to_string(cstr: *const i8) -> Result<String, FromUtf8Error> {
    if cstr == null() {
        return Ok(String::new());
    }

    let mut vec = Vec::new();
    let mut i = 0;
    while *cstr.offset(i) != 0 {
        vec.push(*cstr.offset(i) as u8);
        i += 1;
    }
    String::from_utf8(vec)
}
